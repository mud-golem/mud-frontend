Instructions for setting up the frontend of mud-golem
=====================================================

The frontend of mud-golem is completly written with Vue and JS and therefore does not any specific Webserver or Runtime.

During development apache2 was used as the webserver to distribute the frontend to users.



Step 1
=====================================================
Update system and install dependencies

# apt update
# apt upgrade
# apt install git apache2 npm



Step 2
=====================================================
Download frontend repo.

# git clone https://bitbucket.org/mud-golem/mud-frontend.git



Step 3
=====================================================
Configure frontend

Before building the frontend, the URL of the API must be set in a config file.

Open the config file in an editor:
# nano mud-frontend/src/config.js

And change the value of "config.apiaddress" to the URL of the API.

Save and exit the file.



Step 4
=====================================================
Building the frontend.

Enter the git repo:
# cd mud-frontend

Get the npm dependencies:
# npm install

Build the project:
# npm run build



Step 5
=====================================================
Deploy the frontend.

Copy the contents of the dist folder to the root of the webserver.
# cp -r dist/. /var/www/html/

!!!Copy the .htaccess file aswell and make sure, that the Rewrite rules work!!!
For apache2: Add following to the relevant virtual host files:
#   <Directory /var/www/html>
#       AllowOverride All
#   </Directory>

Finished
=====================================================
The frontend is now accessible on your webserver.