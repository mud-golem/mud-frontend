import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import Login from '../views/registerLoginView/Login.vue'
import Lobby from '../views/lobbyView/Lobby.vue'
import Register from '../views/registerLoginView/Register.vue'
import MailSent from '../views/registerLoginView/MailSent.vue'
import MailConfirmed from '../views/registerLoginView/MailConfirmed.vue'
import AvatarCreation from '../views/playerView/PlayerAvatarCreation.vue'
import PlayerGame from '../views/playerView/PlayerGame.vue'
import GameDungeonMaster from '../views/dmView/DungeonMasterGame.vue'
import DungeonCopy from '../views/dmView/DungeonCopy.vue'
import DungeonCreation from '../views/lobbyView/DungeonCreation.vue'
import ChatError from '../views/siteView/ChatError.vue'
import PasswordResetRequest from '../views/registerLoginView/PasswordResetRequest.vue'
import PasswordChange from '../views/registerLoginView/PasswordChange.vue'
import ResetRequestSent from '../views/registerLoginView/ResetRequestSent.vue'
import PasswordChangeConfirmation from '../views/registerLoginView/PasswordChangeConfirmation.vue'
import DungeonRemoveConfirmation from '../views/dmView/DungeonRemoveConfirmation.vue'
import PlayerAvatarRemoveConfirmation from '../views/playerView/PlayerAvatarRemoveConfirmation.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/lobby',
    name: 'Lobby',
    component: Lobby
  },
  {
    path: '/mailSent',
    name: 'MailSent',
    component: MailSent
  },
  {
    path: '/mailConfirmed/:token',
    name: 'MailConfirmed',
    component: MailConfirmed
  },
  {
    path: '/gameplayer',
    name: 'PlayerGame',
    component: PlayerGame
  },
  {
    path: '/avatarcreation',
    name: 'AvatarCreation',
    component: AvatarCreation
  },
  {
    path: '/gamedungeonmaster',
    name: 'GameDungeonMaster',
    component: GameDungeonMaster
  },
  {
    path: '/dungeonCreation',
    name: 'DungeonCreation',
    component: DungeonCreation
  },
  {
    path: '/dungeonCopy',
    name: 'DungeonCopy',
    component: DungeonCopy
  },
  {
    path: '/chatError',
    name: 'ChatError',
    component: ChatError
  },
  {
    path: '/passwordResetRequest',
    name: 'PasswordResetRequest',
    component: PasswordResetRequest
  },
  {
    path: '/passwordChange/:token',
    name: 'PasswordChange',
    component: PasswordChange
  },
  {
    path: '/resetRequestSent',
    name: 'ResetRequestSent',
    component: ResetRequestSent
  },
  {
    path: '/passwordChangeConfirmation',
    name: 'PasswordChangeConfirmation',
    component: PasswordChangeConfirmation
  },
  {
    path: '/dungeonRemoveConfirmation',
    name: 'DungeonRemoveConfirmation',
    component: DungeonRemoveConfirmation
  },
  {
    path: '/avatarRemoveConfirmation',
    name: 'PlayerAvatarRemoveConfirmation',
    component: PlayerAvatarRemoveConfirmation
  },
  /*{
    path: '/dungeon/:dungeonId',
    name: 'Lobby',
    component: Lobby
  }*/
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router