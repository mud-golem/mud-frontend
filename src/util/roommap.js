export function calculateMapData(startroomID, rooms) {
    // Convert array to hashmap
    var roomMap = new Map(rooms.map(i => [i.scopedID, { ...i, visited: false, x: 0, y: 0 }]));

    // Do the search
    visitRoom(roomMap, startroomID, 0, 0);

    // Discard unvisited elements
    for (let [key, room] of roomMap.entries()) {
        if (!room.visited)
          roomMap.delete(key);
    }

    // find out what the extreme values are
    var minX = 0;
    var maxX = 0;
    var minY = 0;
    var maxY = 0;
    roomMap.forEach(element => {
        if (element.x < minX)
            minX = element.x;
        if (element.x > maxX)
            maxX = element.x;
        if (element.y < minY)
            minY = element.y;
        if (element.y > maxY)
            maxY = element.y;
    }, roomMap);

    // calculate the number of elements in each direction
    var sizeX = (maxX - minX) + 1;
    var sizeY = (maxY - minY) + 1;

console.log(minX);
console.log(maxX);
console.log(minY);
console.log(maxY);
console.log(roomMap);


    roomMap.forEach(element => {
        // shift all the rooms, so none is smaller than 0
        element.x = element.x + (-minX);
        element.y = element.y + (-minY);

        // Flip the y axis, because origin is at top
        element.y = (sizeY - 1) - element.y

        //Index of grid starts at 1
        element.x += 1;
        element.y += 1;
    }, roomMap);

    console.log(roomMap);

    return {roomMap, sizeX, sizeY};
}

function visitRoom(roomMap, roomID, x, y) {
    var currentRoom = roomMap.get(roomID);
    // Already visited the room?
    if (currentRoom.visited) {
        // Check if the coordinates seem right
        if (currentRoom.x == x && currentRoom.y == y) {
            return;
        } else {
            throw Error("Non euclidean");
        }
    }
    else {
        // room has not been visited, set everything
        currentRoom.visited = true;
        currentRoom.x = x;
        currentRoom.y = y;

        //start visiting its neighbours
        if (currentRoom.roomNorthScopedID > 0)
            visitRoom(roomMap, currentRoom.roomNorthScopedID, x, y + 1);
        if (currentRoom.roomSouthScopedID > 0)
            visitRoom(roomMap, currentRoom.roomSouthScopedID, x, y - 1);
        if (currentRoom.roomEastScopedID > 0)
            visitRoom(roomMap, currentRoom.roomEastScopedID, x + 1, y);
        if (currentRoom.roomWestScopedID > 0)
            visitRoom(roomMap, currentRoom.roomWestScopedID, x - 1, y);
    }
    return;
}