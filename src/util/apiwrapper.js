import axios from "axios";
import {config} from '@/config.js';

const ax = axios.create({
    baseURL: 'https://'+ config.apiaddress,
})

/*
If unexpexted HTTP codes apear, exceptions are thrown
*/

/**
 * @Author Christian Strunk
 * Try to register a new user (Backend sends mail)
 * @param {string} username 
 * @param {string} password 
 * @param {string} mailAddress 
 */
export async function register(username, password, mailAddress) {
    await ax.post('/register', {
        username: username,
        password: password,
        mailAddress: mailAddress
    },
        {
            validateStatus: function (status) {
                return status == 200; // default
            }
        });
}

/**
 * @Author Christian Strunk
 * Try to confirm and register new user
 * @param {*} key register token
 */
export async function confirmMail(key) {
    // send request
    await ax.post('/register/confirm', { key: key }, {
        validateStatus: function (status) {
            return status == 200; // default
        },
    });
}

/**
 * @Author Christian Strunk
 * Try to log in with an existing user
 * @param {string} username 
 * @param {string} password 
 */
export async function login(username, password) {
    // Send request
    const response = await ax.post('/login', {
        username: username,
        password: password,
    }, {
        validateStatus: function (status) {
            return status == 200; // default
        }
    });

    // Read the token and save it
    var token = response.data
    sessionStorage.setItem('SecurityKey', token)
}

/**
 * @Author Christian Strunk
 * Deletes the login token
 */
export async function logout() {
    sessionStorage.removeItem('SecurityKey');
}

/**
 * @Author Christian Strunk
 * Request a password reset
 * @param {string} mailAddress EMail of the user who wants a new password
 * @param {string} userName Username of the user who wants a new password
 */
export async function requestResetPassword(username, mailAddress) {
    // send request
    await ax.post('/login/changePassword', { username: username, mailAddress: mailAddress }, {
        validateStatus: function (status) {
            return status == 200; // default
        },
    });
}

/**
 * @Author Christian Strunk
 * Reset a password
 * @param {string} key Key send along with the reset link
 * @param {string} password The new password
 */
export async function resetPassword(key, password) {
    // send request
    await ax.post('/login/changePassword/confirm', { key: key, password: password }, {
        validateStatus: function (status) {
            return status == 200; // default
        },
    });
}


/**
 * @Author Christian Strunk
 * Trys to create an new avatar in an dungeon
 * @param {CreatedAvatar} createdAvatar avatar object {string uniqueName}
 * @param {string} dungeonID Dungeon where to create the avatar
 * @returns Location to avatar ressource
 */
export async function createAvatar(createdAvatar, dungeonID) {
    // Read to token
    var token = sessionStorage.getItem('SecurityKey');
    if (token == null) {
        throw Error('not logged in');
    }

    // send request
    const response = await ax.post(`/dungeons/${dungeonID}/avatars`, createdAvatar, {
        headers: {
            'SecurityKey': token
        },
        validateStatus: function (status) {
            return status == 201; // default
        }
    });

    // If successful, return location
    return response.headers.Location;
}

/**
 * @Author Christian Strunk
 * Trys to create a new dungeon
 * @param {*} createdDungeon CreatedDungeon Object: {string uniqueName, string lobbyDescription}
 * @returns Location to new dungeon ressource
 */
export async function createDungeon(createdDungeon) {
    // read login token
    var token = sessionStorage.getItem('SecurityKey');
    if (token == null) {
        throw Error('not logged in');
    }

    // post object
    const response = await ax.post('/dungeons', createdDungeon, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 201; // default
        }
    });
    return response.data.Location;
}

/**
 * @Author Christian Strunk
 * Try to copy a dungeon
 * @param {*} oldDungeonName Name of the old dungeon
 * @param {*} newDungeonName Name of the new dungeon
 * @returns Location to new dungeon ressource
 */
 export async function copyDungeon(oldDungeonName, newDungeonName) {
    // read login token
    var token = sessionStorage.getItem('SecurityKey');
    if (token == null) {
        throw Error('not logged in');
    }

    // post object
    const response = await ax.post('/dungeons/copyDungeon', {originalDungeonName: oldDungeonName, newDungeonName: newDungeonName}, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 201; // default
        }
    });
    return response.data.Location;
}

/**
 * @Author Christian Strunk
 * Get a specific dungeon
 * @param {string} ID of the dungeon
 * @returns requested dungeon
 */
export async function getDungeon(id) {
    var token = sessionStorage.getItem('SecurityKey');
    const response = await ax.get(`/dungeons/${id}`, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 200; // default
        }
    });
    return response.data;
}

/**
 * @Author Christian Strunk
 * Send modifications to server
 * @param {*} dungeon a full dungeon object
 */
export async function changeDungeon(dungeon) {
    // read login token
    var token = sessionStorage.getItem('SecurityKey');

    // put object
    await ax.put(`/dungeons/${dungeon.uniqueName}`, dungeon, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 202; // default
        }
    });
}

/*
class LobbyDungeon {
    string uniqueName
    string lobbyDescription
    string dungeonMasterUserName
    number activePlayers
    boolean allowedJoinAsPlayer
    boolean allowedJoinAsDM
}
*/
/**
 * @Author Christian Strunk
 * Get List of all dungeons for lobby
 * @returns List of available Dungeons
 */
export async function getDungeonList() {
    var token = sessionStorage.getItem('SecurityKey');
    const response = await ax.get('/dungeons', {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 200; // default
        }
    });
    return response.data;
}

/**
 * @Author Christian Strunk
 * Get the Name of the avatar form the current user in a certain dungeon
 * @param {string} dungeonID
 * @returns avatar name as string
 */
export async function getAvatar(dungeonID) {
    var token = sessionStorage.getItem('SecurityKey');
    const response = await ax.get(`/dungeons/${dungeonID}/myAvatar`, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 200; // default
        }
    });

    return response.data;
}

/**
 * @Author Christian Strunk
 * Get all names of active avatars in the same room
 * @param {string} dungeonID
 * @returns avatar names in list
 */
 export async function getSameRoomAvatars(dungeonID) {
    var token = sessionStorage.getItem('SecurityKey');
    const response = await ax.get(`/dungeons/${dungeonID}/allAvatarNamesSameRoom`, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 200; // default
        }
    });

    return response.data;
}

/**
 * @Author Christian Strunk
 * Get the Names of the active avatars in the dungeon
 * @param {string} dungeonID
 * @returns avatar name as string
 */
 export async function getAvatarNames(dungeonID) {
    var token = sessionStorage.getItem('SecurityKey');
    const response = await ax.get(`/dungeons/${dungeonID}/allAvatarNames`, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 200; // default
        }
    });

    return response.data;
}

/**
 * @Author Christian Strunk
 * Get List of all classes in dungeon
 * @param {*} dungeonID
 * @returns List Classes
 */
export async function getClasses(dungeonID) {
    var token = sessionStorage.getItem('SecurityKey');
    const response = await ax.get(`/dungeons/${dungeonID}/classes`, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 200; // default
        }
    });
    return response.data;
}

/**
 * @Author Christian Strunk
 * Create a new Class
 * @param {*} createdClass The new Class
 * @returns Location to new class
 */
export async function createClass(dungeonID, createdClass) {
    // read login token
    var token = sessionStorage.getItem('SecurityKey');

    // post object
    const response = await ax.post(`/dungeons/${dungeonID}/classes`, createdClass, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 201; // default
        }
    });
    return response.data.Location;
}

/**
 * @Author Christian Strunk
 * Get a class from a dungeon
 * @param {*} dungeonID
 * @param {*} classID
 * @returns Classes
 */
export async function getClass(dungeonID, classID) {
    var token = sessionStorage.getItem('SecurityKey');
    const response = await ax.get(`/dungeons/${dungeonID}/classes/${classID}`, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 200; // default
        }
    });
    return response.data;
}

/**
 * @Author Christian Strunk
 * Send modifications to server
 * @param {*} dungeonID dungeon of class
 * @param {*} changedClass
 */
export async function changeClass(dungeonID, changedClass) {
    // read login token
    var token = sessionStorage.getItem('SecurityKey');

    // put object
    await ax.put(`/dungeons/${dungeonID}/classes/${changedClass.globalID}`, changedClass, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 202; // default
        }
    });
}

/**
 * @Author Christian Strunk
 * Get List of all races in dungeon
 * @param {*} dungeonID
 * @returns List Races
 */
export async function getRaces(dungeonID) {
    var token = sessionStorage.getItem('SecurityKey');
    const response = await ax.get(`/dungeons/${dungeonID}/races`, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 200; // default
        }
    });
    return response.data;
}

/**
 * @Author Christian Strunk
 * Create a new Race
 * @param {*} createdRace The new Race
 * @returns Location to new Race
 */
export async function createRace(dungeonID, createdRace) {
    // read login token
    var token = sessionStorage.getItem('SecurityKey');

    // post object
    const response = await ax.post(`/dungeons/${dungeonID}/races`, createdRace, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 201; // default
        }
    });
    return response.data.Location;
}

/**
 * @Author Christian Strunk
 * Get a race from a dungeon
 * @param {*} dungeonID
 * @param {*} raceID
 * @returns Race
 */
export async function getRace(dungeonID, raceID) {
    var token = sessionStorage.getItem('SecurityKey');
    const response = await ax.get(`/dungeons/${dungeonID}/races/${raceID}`, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 200; // default
        }
    });
    return response.data;
}

/**
 * @Author Christian Strunk
 * Send modifications to server
 * @param {*} dungeonID dungeon of Race
 * @param {*} changedRace
 */
export async function changeRace(dungeonID, changedRace) {
    // read login token
    var token = sessionStorage.getItem('SecurityKey');

    // put object
    await ax.put(`/dungeons/${dungeonID}/races/${changedRace.globalID}`, changedRace, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 202; // default
        }
    });
}

/**
 * @Author Christian Strunk
 * Get List of all rooms in dungeon
 * @param {*} dungeonID
 * @returns List of RoomListEntrys
 */
export async function getRooms(dungeonID) {
    var token = sessionStorage.getItem('SecurityKey');
    const response = await ax.get(`/dungeons/${dungeonID}/rooms`, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 200; // default
        }
    });
    return response.data;
}

/**
 * @Author Christian Strunk
 * Create a new Room
 * @param {*} createdRoom The new Room
 * @returns Location to new room
 */
export async function createRoom(dungeonID, createdRoom) {
    // read login token
    var token = sessionStorage.getItem('SecurityKey');

    // post object
    const response = await ax.post(`/dungeons/${dungeonID}/rooms`, createdRoom, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 201; // default
        }
    });
    return response.data.Location;
}

/**
 * @Author Christian Strunk
 * Get a room from a dungeon
 * @param {*} dungeonID
 * @param {*} roomID
 * @returns Room
 */
export async function getRoom(dungeonID, roomID) {
    var token = sessionStorage.getItem('SecurityKey');
    const response = await ax.get(`/dungeons/${dungeonID}/rooms/${roomID}`, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 200; // default
        }
    });
    return response.data;
}

/**
 * @Author Christian Strunk
 * Send modifications to server
 * @param {*} dungeonID dungeon of room
 * @param {*} changedRoom
 */
export async function changeRoom(dungeonID, changedRoom) {
    // read login token
    var token = sessionStorage.getItem('SecurityKey');

    // put object
    await ax.put(`/dungeons/${dungeonID}/rooms/${changedRoom.globalID}`, changedRoom, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 202; // default
        }
    });
}

/**
* @Author Christian Strunk
* remove avatar from dungeon
* @param {*} avatarID Id of the avatar
*/
export async function removeRoom(dungeonID, roomID) {
    // read login token
    var token = sessionStorage.getItem('SecurityKey');
 
    // post object
    const response = await ax.delete(`/dungeons/${dungeonID}/rooms/${roomID}`, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 204; // default
        }
    });
    return response.data;
}

/**
 * @Author Marcel Schaefer
 * Create a new object template
 * @param {*} dungeonID dungeon of object
 * @param {*} createdObject new object template
 */
 export async function createObject(dungeonID, createdObject) {
    // read login token
    var token = sessionStorage.getItem('SecurityKey');

    // post object
    const response = await ax.post(`/dungeons/${dungeonID}/objects`, createdObject, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 201; // default
        }
    });
    return response.data.Location;
}

/**
 * @Author Marcel Schaefer
 * Send object modifications to server
 * @param {*} dungeonID dungeon of object
 * @param {*} changedObject
 */
 export async function changeObject(dungeonID, changedObject) {
    // read login token
    var token = sessionStorage.getItem('SecurityKey');

    // put object
    await ax.put(`/dungeons/${dungeonID}/objects/${changedObject.globalID}`, changedObject, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 202; // default
        }
    });
}

/**
 * @Author Marcel Schaefer
 * Get List of all objectTemplates in dungeon
 * @param {*} dungeonID
 * @returns List Objects
 */
 export async function getObjects(dungeonID) {
    var token = sessionStorage.getItem('SecurityKey');
    const response = await ax.get(`/dungeons/${dungeonID}/objects`, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 200; // default
        }
    });
    return response.data;
}

/**
 * @Author Christian Strunk
 * Send taken action to server
 * @param {*} dungeonID dungeon of room
 * @param {*} action action thats been send
 */
 export async function sendAction(dungeonID, action) {
    // read login token
    var token = sessionStorage.getItem('SecurityKey');

    // put object
    await ax.put(`/dungeons/${dungeonID}/action`, action, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 202; // default
        }
    });
}

/**
 * @Author Marcel Schaefer
 * Get List of all Avatars in dungeon
 * @param {*} dungeonID
 * @returns List Avatars
 */
 export async function getAllAvatars(dungeonID) {
    var token = sessionStorage.getItem('SecurityKey');
    const response = await ax.get(`/dungeons/${dungeonID}/avatars`, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 200; // default
        }
    });
    return response.data;
}

/**
 * @Author Marcel Schaefer
 * Get a selected avatar from a dungeon as a DM
 * @param {*} dungeonID
 * @param {*} avatarID
 * @returns Avatar
 */
 export async function getSelectedAvatar(dungeonID, avatarID) {
    var token = sessionStorage.getItem('SecurityKey');
    const response = await ax.get(`/dungeons/${dungeonID}/avatars/${avatarID}`, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 200; // default
        }
    });
    return response.data;
}

/**
 * @Author Marcel Schaefer
 * Send avatar modifications to server
 * @param {*} dungeonID dungeon of object
 * @param {*} changedAvatar
 */
 export async function changeAvatar(dungeonID, changedAvatar) {
    // read login token
    var token = sessionStorage.getItem('SecurityKey');
    console.log(changedAvatar);
    // put object
    await ax.put(`/dungeons/${dungeonID}/avatars/${changedAvatar.globalID}`, changedAvatar, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 202; // default
        }
    });
}

/**
* @Author Christian Strunk
* remove avatar from dungeon
* @param {*} avatarID Id of the avatar
*/
export async function removeAvatar(dungeonID, avatarID) {
    // read login token
    var token = sessionStorage.getItem('SecurityKey');
 
    // post object
    const response = await ax.delete(`/dungeons/${dungeonID}/avatars/${avatarID}`, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 204; // default
        }
    });
    return response.data;
}

/**
 * @Author Christian Strunk
 * Get List of all Actions in dungeon
 * @param {*} dungeonID
 * @returns List Actions
 */
 export async function getActions(dungeonID) {
    var token = sessionStorage.getItem('SecurityKey');
    const response = await ax.get(`/dungeons/${dungeonID}/action`, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 200; // default
        }
    });
    return response.data;
}

/**
 * @Author Christian Strunk
 * Create a new action
 * @param {*} createdAction The new action
 * @returns Location to new action
 */
export async function createAction(dungeonID, createdAction) {
    // read login token
    var token = sessionStorage.getItem('SecurityKey');

    // post object
    const response = await ax.post(`/dungeons/${dungeonID}/action`, createdAction, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 201; // default
        }
    });
    return response.data.Location;
}

/**
 * @Author Christian Strunk
 * Get a action from a dungeon
 * @param {*} dungeonID
 * @param {*} actionID
 * @returns Action
 */
export async function getAction(dungeonID, actionID) {
    var token = sessionStorage.getItem('SecurityKey');
    const response = await ax.get(`/dungeons/${dungeonID}/action/${actionID}`, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 200; // default
        }
    });
    return response.data;
}

/**
 * @Author Christian Strunk
 * Send modifications to server
 * @param {*} dungeonID dungeon of Race
 * @param {*} changedAction
 */
export async function changeAction(dungeonID, changedAction) {
    // read login token
    var token = sessionStorage.getItem('SecurityKey');

    // put object
    await ax.put(`/dungeons/${dungeonID}/action/${changedAction.globalID}`, changedAction, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 202; // default
        }
    });
}

/**
 * @Author Christian Strunk
 * Get blacklist
 * @param {*} dungeonID
 */
 export async function getBlacklist(dungeonID) {
    var token = sessionStorage.getItem('SecurityKey');
    const response = await ax.get(`/dungeons/${dungeonID}/blacklist`, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 200; // default
        }
    });
    return response.data;
}

/**
 * @Author Christian Strunk
 * Add user to blacklist
 * @param {*} username The username of the username
 */
 export async function addToBlacklist(dungeonID, username) {
    // read login token
    var token = sessionStorage.getItem('SecurityKey');

    // post object
    const response = await ax.post(`/dungeons/${dungeonID}/blacklist`, {username: username}, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 201; // default
        }
    });
    return response.data;
}

/**
 * @Author Christian Strunk
 * remove user from blacklist
 * @param {*} username The username of the user
 */
 export async function removeFromBlacklist(dungeonID, username) {
    // read login token
    var token = sessionStorage.getItem('SecurityKey');

    // post object
    const response = await ax.delete(`/dungeons/${dungeonID}/blacklist/${username}`, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 204; // default
        }
    });
    return response.data;
}

/**
 * @Author Christian Strunk
 * Get whitelist
 * @param {*} dungeonID
 */
 export async function getWhitelist(dungeonID) {
    var token = sessionStorage.getItem('SecurityKey');
    const response = await ax.get(`/dungeons/${dungeonID}/whitelist`, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 200; // default
        }
    });
    return response.data;
}

/**
 * @Author Christian Strunk
 * Add user to whitelist
 * @param {*} username The username of the user
 */
 export async function addToWhitelist(dungeonID, username) {
    // read login token
    var token = sessionStorage.getItem('SecurityKey');

    // post object
    const response = await ax.post(`/dungeons/${dungeonID}/whitelist`, {username: username}, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 201; // default
        }
    });
    return response.data;
}

/**
 * @Author Christian Strunk
 * remove user from whitelist
 * @param {*} username The username of the user
 */
 export async function removeFromWhitelist(dungeonID, username) {
    // read login token
    var token = sessionStorage.getItem('SecurityKey');

    // post object
    const response = await ax.delete(`/dungeons/${dungeonID}/whitelist/${username}`, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 204; // default
        }
    });
    return response.data;
}

/**
* @Author Christian Strunk
* Get DMlist
* @param {*} dungeonID
*/
export async function getDMList(dungeonID) {
   var token = sessionStorage.getItem('SecurityKey');
   const response = await ax.get(`/dungeons/${dungeonID}/dmlist`, {
       headers: {
           'SecurityKey': token,
       },
       validateStatus: function (status) {
           return status == 200; // default
       }
   });
   return response.data;
}

/**
* @Author Christian Strunk
* Add user to DMlist
* @param {*} username The username of the user
*/
export async function addToDMlist(dungeonID, username) {
   // read login token
   var token = sessionStorage.getItem('SecurityKey');

   // post object
   const response = await ax.post(`/dungeons/${dungeonID}/dmlist`, {username: username}, {
       headers: {
           'SecurityKey': token,
       },
       validateStatus: function (status) {
           return status == 201; // default
       }
   });
   return response.data;
}

/**
* @Author Christian Strunk
* remove user from DMlist
* @param {*} username The username of the user
*/
export async function removeFromDMlist(dungeonID, username) {
   // read login token
   var token = sessionStorage.getItem('SecurityKey');

   // post object
   const response = await ax.delete(`/dungeons/${dungeonID}/dmlist/${username}`, {
       headers: {
           'SecurityKey': token,
       },
       validateStatus: function (status) {
           return status == 204; // default
       }
   });
   return response.data;
}


/**
* @Author Christian Strunk
* Get JoinRequests
* @param {*} dungeonID
*/
export async function getJoinRequests(dungeonID) {
    var token = sessionStorage.getItem('SecurityKey');
    const response = await ax.get(`/dungeons/${dungeonID}/joinRequests`, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 200; // default
        }
    });
    return response.data;
 }
 
 /**
 * @Author Christian Strunk
 * Add user to JoinRequests
 * @param {*} username The username of the user
 */
 export async function addToJoinRequests(dungeonID) {
    // read login token
    var token = sessionStorage.getItem('SecurityKey');
 
    // post object
    const response = await ax.post(`/dungeons/${dungeonID}/joinRequests`, {}, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 201; // default
        }
    });
    return response.data;
 }

 /**
 * @Author Christian Strunk
 * accept or decline a request
 * @param {*} isAccepted was the request accepted?
 */
export async function answerJoinRequest(dungeonID, username, isAccepted) {
    // read login token
    var token = sessionStorage.getItem('SecurityKey');

    // put object
    await ax.put(`/dungeons/${dungeonID}/joinRequests/${username}`, {isAccepted: isAccepted}, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 202; // default
        }
    });
}

/**
* @Author Marcel Schaefer
* remove dungeon
* @param {*} dungeonID ID of dungeon
*/
export async function removeDungeon(dungeonID) {
    // read login token
    var token = sessionStorage.getItem('SecurityKey');
 
    // post object
    const response = await ax.delete(`/dungeons/${dungeonID}`, {
        headers: {
            'SecurityKey': token,
        },
        validateStatus: function (status) {
            return status == 204; // default
        }
    });
    return response.data;
}