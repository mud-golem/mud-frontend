import {config} from '@/config.js';

// Base URL for the endpoints
const dmSocketBaseURL = "wss://"+ config.apiaddress +"/dungeonMaster/chat";
const playerSocketBaseURL = "wss://"+ config.apiaddress +"/chat";

/**
 * @author Christian Strunk
 * Creates a connection to the Chatserver
 * @param {string} dungeonName 
 * @param {boolean} asDM 
 * @param {function} onOpenHandler 
 * @param {function} onErrorHandler 
 * @param {*function} onCloseHandler 
 * @returns Webscoket
 */
export function connectToChat(dungeonName, asDM, onOpenHandler, onMessageHandler, onErrorHandler, onCloseHandler) {

    // read the local token for login
    var token = sessionStorage.getItem('SecurityKey');

    // Chose the right base url
    var baseURL;
    if (asDM) {
        baseURL = dmSocketBaseURL;
    } else {
        baseURL = playerSocketBaseURL;
    }

    // Make the path work
    dungeonName = encodeURIComponent(dungeonName);
    token = encodeURIComponent(token);

    // Create the socket and assign the handlers
    var socket = new WebSocket(baseURL + `/${dungeonName}/${token}`);
    socket.onmessage = onMessageHandler;
    socket.onopen = onOpenHandler;
    socket.onerror = onErrorHandler;
    socket.onclose = onCloseHandler;
    return socket;
}

/**
 * @author Christian Strunk
 * Creates a message for a dungeonwide broadcast. Only DMs are able to use this
 * @param {string} content text of the message
 * @returns a message object 
 */
export function createDungeonChatMessage(content){
    var msg = {
        type: "DungeonChatMessage",
        sender: null,
        recipient: null,
        room: null,
        object: null,
        actionName: null,
        priority: 0,
        content: content,
    };

    return msg;
}

/**
 * @author Christian Strunk
 * Creates a message for a room wide broadcast. Both players and DMs use this.
 * For DMs the room must be set.
 * For Players the room is determined by the backend
 * @param {string} room Room set to receive the message, only DM
 * @param {string} content text of the message
 * @returns a message object
 */
export function createRoomChatMessage(room, roomName, content){
    var msg = {
        type: "RoomChatMessage",
        sender: null,
        recipient: null,
        room: room,
        object: null,
        actionName: roomName,
        priority: 0,
        content: content,
    };
    return msg;
}

/**
 * @author Christian Strunk
 * Creates a message for a direct conversation. Can be used by DM and Players
 * @param {string} recipient Avatar who should receive the message
 * @param {string} content text of the message
 * @returns a message object
 */
export function createWhisperChatMessage(recipient, content){
    var msg = {
        type: "WhisperChatMessage",
        sender: null,
        recipient: recipient,
        room: null,
        object: null,
        actionName: null,
        priority: 0,
        content: content,
    };

    return msg;
}

/**
 * @author Christian Strunk
 * Creates a message for a dungeon wide system message. Only for DM
 * @param {string} content text of the message
 * @returns a message object
 */
export function createDungeonSystemMessage(content){
    var msg = {
        type: "DungeonSystemMessage",
        sender: null,
        recipient: null,
        room: null,
        object: null,
        actionName: null,
        priority: 3,
        content: content,
    };

    return msg;
}

/**
 * @author Christian Strunk
 * Creates a message for a room wide system messaeg. Only for DM.
 * @param {string} room Room set to receive the message
 * @param {string} content text of the message
 * @returns a message object
 */
export function createRoomSystemMessage(room, roomName, content){
    var msg = {
        type: "RoomSystemMessage",
        sender: null,
        recipient: null,
        room: room,
        object: null,
        actionName: roomName,
        priority: 3,
        content: content,
    };

    return msg;
}

/**
 * @author Christian Strunk
 * Creates a message for a direct system message. Only for DM.
 * @param {string} recipient Avatar set to receive the message
 * @param {string} content text of the message
 * @returns a message object
 */
export function createWhisperSystemMessage(recipient, content){
    var msg = {
        type: "WhisperSystemMessage",
        sender: null,
        recipient: recipient,
        room: null,
        object: null,
        actionName: null,
        priority: 3,
        content: content,
    };

    return msg;
}

/**
 * @author Christian Strunk
 * Echo for actions
 * @param {string} content text of the message
 * @returns a message object
 */
 export function createActionEcho(content){
    var msg = {
        type: "WhisperSystemMessage",
        sender: null,
        recipient: null,
        room: null,
        object: null,
        actionName: null,
        priority: 0,
        content: content,
    };

    return msg;
}