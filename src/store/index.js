import Vue from 'vue'
import Vuex from 'vuex'
import chat from './modules/chat'
import dungeon from './modules/dungeon'
import room from './modules/room'
import avatarrace from './modules/avatarrace'
import avatarclass from './modules/avatarclass'
import objecttemplate from './modules/objecttemplate'
import action from './modules/action'
import avatar from './modules/avatar'
import user from './modules/user'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    chat,
    dungeon,
    room,
    avatarclass,
    avatarrace,
    objecttemplate,
    avatar,
    action,
    user,
  },
  state: {},
  mutations: {},
  actions: {},
  getters: {}
})
