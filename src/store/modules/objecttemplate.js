import * as api from '@/util/apiwrapper.js'

// initial state
const state = () => (
    {
        // all of the objectTemplates in the dungeon
        objectTemplates: [],
        // currently selected objectTemplate
        selectedObjectTemplate: {
            globalID: -1,
            name: "",
            fullDescription: "",
            isPickupable: false,
            actionIDs: [],
        },
    }
)

// getters
const getters = {
    availableObjectTemplates: state => {
        return state.objectTemplates;
    }
}

// actions
const actions = {
    /**
     * @author Marcel Schaefer
     * @author Christian Strunk
     * load list of objectTemplates form server
     * @param {*} dungeonID ID of the current dungeon
     */
    async loadObjects({ commit }, dungeonID) {
        var objectTemplates = await api.getObjects(dungeonID);
        commit('SET_OBJECTS', objectTemplates);
    },
    /**
     * @author Christian Strunk
     * @author Marcel Schaefer
     * Clear the local data
     */
    async clearObjects({ commit }) {
        commit('CLEAR_OBJECTS');
        commit('CLEAR_SELECTEDOBJECT');
    },
    /**
     * @author Christian Strunk
     * @author Marcel Schaefer
     * Copy the selected object from local data to selected object
     * @param {*} objectID ID of the selected object
     */
    loadSelectedObject({ commit, state, rootState }, objectID) {
        if (objectID < 1) {
            commit('CLEAR_SELECTEDOBJECT');
            return;
        }

        console.log(objectID);
        var objectTemplate = state.objectTemplates.find(o => o.globalID == objectID);
        // copy the object
        var objectTemplateCopy = {
            globalID: objectTemplate.globalID,
            name: objectTemplate.name,
            fullDescription: objectTemplate.fullDescription,
            isPickupable: objectTemplate.isPickupable,
            actionIDs: [...objectTemplate.actionIDs],
        };
        var currentActions = new Map(objectTemplateCopy.actionIDs.map(i => [i, i]));

        objectTemplateCopy.actionIDs = rootState.action.actions.map(element => {
            var currentAction = currentActions.get(element.globalID);
            if (currentAction == undefined) {
                return { globalID: element.globalID, enabled: false, name: element.name };
            } else {
                return { globalID: element.globalID, enabled: true, name: element.name };
            }
        });

        commit('SET_SELECTEDOBJECT', objectTemplateCopy);
    },
    /**
    * @author Christian Strunk
    * @author Marcel Schaefer
    * Update an object
    * @param {*} dungeonID ID of the current dungeon
    */
    async updateObject({ state }, dungeonID) {
        var objectTemplate = state.selectedObjectTemplate;
        var objectTemplateCopy = {
            globalID: objectTemplate.globalID,
            name: objectTemplate.name,
            fullDescription: objectTemplate.fullDescription,
            isPickupable: objectTemplate.isPickupable,
            actionIDs: [...objectTemplate.actionIDs],
        };
        var actionIDs = objectTemplateCopy.actionIDs;
        actionIDs = actionIDs.filter(el => el.enabled).map(el => { return el.globalID });
        objectTemplateCopy.actionIDs = actionIDs;
        await api.changeObject(dungeonID, objectTemplateCopy);
    },

    /**
     * @author Christian Strunk
     * @author Marcel Schäfer
     * Create a new object
     * @param {*} dungeonID ID of the current dungeon
     */
    async createObject({ state }, dungeonID) {
        var objectTemplate = state.selectedObjectTemplate;
        var objectTemplateCopy = {
            globalID: objectTemplate.globalID,
            name: objectTemplate.name,
            fullDescription: objectTemplate.fullDescription,
            isPickupable: objectTemplate.isPickupable,
            actionIDs: [...objectTemplate.actionIDs],
        };
        var actionIDs = objectTemplateCopy.actionIDs;
        actionIDs = actionIDs.filter(el => el.enabled).map(el => { return el.globalID });
        objectTemplateCopy.actionIDs = actionIDs;
        await api.createObject(dungeonID, objectTemplateCopy);
    }
}

// mutations
const mutations = {
    SET_OBJECTS(state, objectTemplates) {
        state.objectTemplates = objectTemplates;
    },
    CLEAR_OBJECTS(state) {
        state.objectTemplates = [];
    },
    SET_SELECTEDOBJECT(state, pobject) {
        state.selectedObjectTemplate = pobject;
    },
    CLEAR_SELECTEDOBJECT(state) {
        state.selectedObjectTemplate = {
            globalID: -1,
            name: "",
            fullDescription: "",
            isPickupable: false,
            actionIDs: [],
        };
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}