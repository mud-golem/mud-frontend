import * as chat from '@/util/chatclient.js'

// initial state
// shape: [{ id, quantity }]
const state = () => (
    {
        // connection to the chat
        chatsocket: {},
        // all of the messages received until this point
        messages: [],
        // the dungeon object for the dm
    }
)

// getters
const getters = {
    // The messages for the Chatlog
    chatMessages: state => {
        return state.messages.filter(msg => ["DungeonChatMessage", "RoomChatMessage", "WhisperChatMessage"].includes(msg.type));
    },
    // The messages for the System / action log
    systemMessages: state => {
        return state.messages.filter(msg => ["DungeonSystemMessage", "RoomSystemMessage", "WhisperSystemMessage", "ActionAlertMessage"].includes(msg.type));
    },
}

// actions
const actions = {
    /**
     * @Author Christian Strunk
     * Connect the local Socket to the server as an Player, set all of the handlers.
     * Takes the dungeonname, in whos chat to join
     */
    async connectToChat({ commit }, { dungeonName, asDM, openHandler, errorHandler, closeHandler }) {
        // Clear the message log
        commit('CLEAR_MESSAGES');

        // What happens when a message arrives
        var messageHandler = event => {
            // Read the message and add it to the local message log
            var msg = JSON.parse(event.data);
            console.log(msg);
            commit('ADD_MESSAGE', msg);
        }

        // create the socket
        var chatsocket = await chat.connectToChat(dungeonName, asDM, openHandler, messageHandler, errorHandler, closeHandler);
        commit('SET_CHATSOCKET', chatsocket);
    },

    /**
    * @Author Christian Strunk
    * Try to send a message in the chat
    */
    async sendMessage({ state }, msg) {
        var s = JSON.stringify(msg);
        console.log(s);
        state.chatsocket.send(s);
    },

    /**
     * @author Christian Strunk
     * Close the websocket
     */
    async closeChat({ commit }) {
        commit('CLEAR_CHATSOCKET');
    },

    /**
     * @author Christian Strunk
     * Used to echo a message to the actionlog
     */
    echo({commit}, content) {
        var msg = chat.createActionEcho(content);
        commit('ADD_MESSAGE', msg);
    }
}

// mutations
const mutations = {
    ADD_MESSAGE(state, msg) {
        state.messages.push(msg)
    },
    CLEAR_MESSAGES(state) {
        state.messages = []
    },
    SET_CHATSOCKET(state, socket) {
        state.chatsocket = socket;
    },
    CLEAR_CHATSOCKET(state) {
        state.chatsocket.close();
        state.chatsocket = {};
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}