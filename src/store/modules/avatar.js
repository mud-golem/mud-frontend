import * as api from '@/util/apiwrapper.js'

// everything for avatars


// initial state
const state = () => (
    {
        // AvatarListEntry of all the avatars in the dungeon
        avatars: [],
        // the currently selected avatar
        selectedAvatar: {
            globalID: -1,
        },
    }
)

// getters
const getters = {
}

// actions
const actions = {
    /**
     * @author Marcel Schaefer
     * Load list of avatars from the server
     * @param {*} dungeonID ID of the current dungeon
     */
    async loadAvatars({ commit }, dungeonID) {
        var avatars = await api.getAllAvatars(dungeonID);
        commit('SET_AVATARS', avatars);
    },
    /**
     * @author Marcel Schaefer
     * Clear the localy saved avatars
     */
    async clearAvatars({ commit }) {
        commit('CLEAR_AVATARS');
        commit('CLEAR_SELECTEDAVATAR');
    },
    /**
     * @author Marcel Schaefer
     * @author Christian Strunk
     * Load a specific avatar from the server
     * @param {*} dungeonID ID of the current dungeon
     * * @param {*} avatarID ID of the avatar
     */
    async loadSelectedAvatar({ commit, rootState }, { dungeonID, avatarID}) {
        if (avatarID < 1) {
            commit('CLEAR_SELECTEDAVATAR');
            return;
        }

        var avatar = await api.getSelectedAvatar(dungeonID, avatarID);
        console.log(avatar);
        // create hashmap based on inventory entries
        var currentInventory = new Map(avatar.inventory.map(i => [i.objectTemplateGlobalID, i]));
        avatar.inventory = rootState.objecttemplate.objectTemplates.map(element => {
            var currentInventoryItem = currentInventory.get(element.globalID);
            if (currentInventoryItem == undefined) {
                return {objectTemplateGlobalID: element.globalID, count: 0, displayName: element.name};
            } else {
                return {objectTemplateGlobalID: element.globalID, count: currentInventoryItem.count, displayName: element.name};
            }
        });
        commit('SET_SELECTEDAVATAR', avatar);
    },
    /**
     * @author Marcel Schaefer
     * Send the selected Avatar to the server
     * @param {*} dungeonID ID of the current dungeon
     */
    async updateAvatar({ state }, dungeonID) {
        await api.changeAvatar(dungeonID, state.selectedAvatar);
    },

    async removeAvatar({state}, dungeonID) {
        await api.removeAvatar(dungeonID, state.selectedAvatar.globalID);
    }
}

// mutations
const mutations = {
    SET_AVATARS(state, avatars) {
        state.avatars = avatars;
    },
    CLEAR_AVATARS(state) {
        state.avatars = [];
    },
    SET_SELECTEDAVATAR(state, avatar) {
        state.selectedAvatar = avatar;
    },
    CLEAR_SELECTEDAVATAR(state) {
        state.selectedAvatar = {
            globalID: -1,
        };
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}