import * as api from '@/util/apiwrapper.js'

// initial state
const state = () => (
    {
        //all of the races in the dungeon
        races: [],
        // currently selected race
        selectedRace: {
            globalID: -1,
            displayName: "",
            description: "",
            HPModifier: 0,
            isEnabled: false,
        },
    }
)

// getters
const getters = {
    // the races a new player can choose from
    availableRaces: state => {
        return state.races.filter(race => race.isEnabled);
    },
}

// actions
const actions = {
    /**
     * @author Christian Strunk
     * load list of races form server
     * @param {*} dungeonID ID of the current dungeon
     */
    async loadRaces({ commit }, dungeonID) {
        console.log(dungeonID);
        var races = await api.getRaces(dungeonID);
        commit('SET_RACES', races);
    },

    /**
     * @author Christian Strunk
     * Clear the local data
     */
    async clearRaces({ commit }) {
        commit('CLEAR_RACES');
        commit('CLEAR_SELECTEDRACE');
    },

    /**
     * @author Christian Strunk
     * Copy the selected race from local data to selected race
     * @param {*} raceID ID of the selected race
     */
    loadSelectedRace({ commit, state }, raceID) {
        if (raceID < 1) {
            commit('CLEAR_SELECTEDRACE');
            return;
        }

        var race = state.races.find(r => r.globalID == raceID);
        console.log(race);
        commit('SET_SELECTEDRACE', {
            globalID: race.globalID,
            displayName: race.displayName,
            description: race.description,
            HPModifier: race.HPModifier,
            isEnabled: race.isEnabled,
        });
    },

    /**
     * @author Christian Strunk
     * Update a race
     * @param {*} dungeonID ID of the current dungeon
     */
    async updateRace({ state }, dungeonID) {
        await api.changeRace(dungeonID, state.selectedRace);
    },

    /**
     * @author Christian Strunk
     * Create a new race
     * @param {*} dungeonID ID of the current dungeon
     */
    async createRace({ state }, dungeonID) {
        var tempRace = state.selectedRace;
        tempRace.globalID = null;
        await api.createRace(dungeonID, tempRace);
    }
}

// mutations
const mutations = {
    SET_RACES(state, races) {
        state.races = races;
    },
    CLEAR_RACES(state) {
        state.races = [];
    },
    SET_SELECTEDRACE(state, race) {
        state.selectedRace = race;
    },
    CLEAR_SELECTEDRACE(state) {
        state.selectedRace = {
            globalID: -1,
            displayName: "",
            description: "",
            HPModifier: 0,
            isEnabled: false,
        };
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}