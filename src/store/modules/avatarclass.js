import * as api from '@/util/apiwrapper.js'

// initial state
const state = () => (
    {
        // all of the classes in the dungeon
        classes: [],
        // currently selected class
        selectedClass: {
            globalID: -1,
            displayName: "",
            description: "",
            HPModifier: 0,
            isEnabled: false,
        },
    }
)

// getters
const getters = {
    availableClasses: state => {
        return state.classes.filter(avatarClass => avatarClass.isEnabled);
    }
}

// actions
const actions = {
    /**
     * @author Christian Strunk
     * load list of classes form server
     * @param {*} dungeonID ID of the current dungeon
     */
    async loadClasses({ commit }, dungeonID) {
        var classes = await api.getClasses(dungeonID);
        commit('SET_CLASSES', classes);
    },
    /**
     * @author Christian Strunk
     * Clear the local data
     */
    async clearClasses({ commit }) {
        commit('CLEAR_CLASSES');
        commit('CLEAR_SELECTEDCLASS');
    },
    /**
     * @author Christian Strunk
     * Copy the selected class from local data to selected class
     * @param {*} classID ID of the selected class
     */
    loadSelectedClass({ commit, state }, classID) {
        if (classID < 1) {
            commit('CLEAR_SELECTEDCLASS');
            return;
        }

        var avatarClass = state.classes.find(c => c.globalID == classID);
        console.log(avatarClass);
        commit('SET_SELECTEDCLASS', {
            globalID: avatarClass.globalID,
            displayName: avatarClass.displayName,
            description: avatarClass.description,
            HPModifier: avatarClass.HPModifier,
            isEnabled: avatarClass.isEnabled,
        });
    },
     /**
     * @author Christian Strunk
     * Update a class
     * @param {*} dungeonID ID of the current dungeon
     */
    async updateClass({ state }, dungeonID) {
        await api.changeClass(dungeonID, state.selectedClass);
    },

    /**
     * @author Christian Strunk
     * Create a new class
     * @param {*} dungeonID ID of the current dungeon
     */
    async createClass({ state }, dungeonID) {
        var tempClass = state.selectedClass;
        tempClass.globalID = null;
        await api.createClass(dungeonID, tempClass);
    }
}

// mutations
const mutations = {
    SET_CLASSES(state, classes) {
        state.classes = classes;
    },
    CLEAR_CLASSES(state) {
        state.classes = [];
    },
    SET_SELECTEDCLASS(state, pclass) {
        state.selectedClass = pclass;
    },
    CLEAR_SELECTEDCLASS(state) {
        state.selectedClass = {
            globalID: -1,
            displayName: "",
            description: "",
            HPModifier: 0,
            isEnabled: false,
        };
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}