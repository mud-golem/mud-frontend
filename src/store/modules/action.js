import * as api from '@/util/apiwrapper.js'

// initial state
const state = () => (
    {
        //all of the actions in the dungeon
        actions: [],
        // currently selected action
        selectedAction: {
            globalID: null,
            name: "",
            priority: 1,
            command: "",
            response: "",
        },
    }
)

// getters
const getters = {
}

// actions
const actions = {
    /**
     * @author Christian Strunk
     * load list of actions form server
     * @param {*} dungeonID ID of the current dungeon
     */
    async loadActions({ commit }, dungeonID) {
        console.log(dungeonID);
        var actions = await api.getActions(dungeonID);
        commit('SET_ACTIONS', actions);
    },

    /**
     * @author Christian Strunk
     * Clear the local data
     */
    async clearActions({ commit }) {
        commit('CLEAR_ACTIONS');
        commit('CLEAR_SELECTEDACTION');
    },

    /**
     * @author Christian Strunk
     * Copy the selected action from local data to selected action
     * @param {*} actionID ID of the selected action
     */
    loadSelectedAction({ commit, state }, actionID) {
        if (actionID < 1) {
            commit('CLEAR_SELECTEDACTION');
            return;
        }

        var action = state.actions.find(r => r.globalID == actionID);
        console.log(action);
        commit('SET_SELECTEDACTION', {
            globalID: action.globalID,
            name: action.name,
            priority: action.priority,
            command: action.command,
            response: action.response,
        });
    },

    /**
     * @author Christian Strunk
     * Update a action
     * @param {*} dungeonID ID of the current dungeon
     */
    async updateAction({ state }, dungeonID) {
        await api.changeAction(dungeonID, state.selectedAction);
    },

    /**
     * @author Christian Strunk
     * Create a new action
     * @param {*} dungeonID ID of the current dungeon
     */
    async createAction({ state }, dungeonID) {
        var tempAction = state.selectedAction;
        tempAction.globalID = null;
        await api.createAction(dungeonID, tempAction);
    }
}

// mutations
const mutations = {
    SET_ACTIONS(state, actions) {
        state.actions = actions;
    },
    CLEAR_ACTIONS(state) {
        state.actions = [];
    },
    SET_SELECTEDACTION(state, action) {
        state.selectedAction = action;
    },
    CLEAR_SELECTEDACTION(state) {
        state.selectedAction = {
            globalID: null,
            name: "",
            priority: 1,
            command: "",
            response: "",
        };
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}