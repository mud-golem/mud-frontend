import * as api from '@/util/apiwrapper.js'

// everything for the dungeon

// initial state
const state = () => (
    {
        dungeon: {},
    }
)

// getters
const getters = {
}

// actions
const actions = {
    /**
     * @author Christian Strunk
     * load the dungeon from the server
     * @param {*} dungeonID ID of the current dungeon
     */
    async loadDungeon({ commit, rootState }, dungeonID) {
        var dungeon = await api.getDungeon(dungeonID);
        // copy the object
        var currentActions = new Map(dungeon.actionIDs.map(i => [i, i]));
        console.log(currentActions);
        dungeon.actionIDs = rootState.action.actions.map(element => {
            var currentAction = currentActions.get(element.globalID);
            if (currentAction == undefined) {
                return { globalID: element.globalID, enabled: false, name: element.name };
            } else {
                return { globalID: element.globalID, enabled: true, name: element.name };
            }
        });

        commit('SET_DUNGEON', dungeon);
    },

    /**
     * @author Christian Strunk
     * Clear the local object
     */
    clearDungeon({ commit }) {
        commit('CLEAR_DUNGEON');
    },

    /**
     * @author Christian Strunk
     * Send the dungeon object to the server
     */
    async updateDungeon({ state }) {
        var dungeon = state.dungeon;
        var dungeonCopy = {
            uniqueName: dungeon.uniqueName,
            dungeonDescription: dungeon.dungeonDescription,
            lobbyDescription: dungeon.lobbyDescription,
            startRoomScopedID: dungeon.startRoomScopedID,
            unknownActionResponse: dungeon.unknownActionResponse,
            dungeonMasterUserName: dungeon.dungeonMasterUserName,
            isJoinable: dungeon.isJoinable,
            useWhitelistInsteadOfBlacklist: dungeon.useWhitelistInsteadOfBlacklist,
            whitelistUserNames: [...dungeon.whitelistUserNames],
            blacklistUserNames: [...dungeon.blacklistUserNames],
            joinRequestUserNames: [...dungeon.joinRequestUserNames],
            DMUserNames: [...dungeon.DMUserNames],
            actionIDs: [...dungeon.actionIDs],
        };
        var actionIDs = dungeonCopy.actionIDs;
        actionIDs = actionIDs.filter(el => el.enabled).map(el => { return el.globalID });
        dungeonCopy.actionIDs = actionIDs;
        await api.changeDungeon(dungeonCopy);
        //await state.dispatch('loadDungeon', dungeonCopy.uniqueName);
    },
}

// mutations
const mutations = {
    SET_DUNGEON(state, dungeon) {
        state.dungeon = dungeon;
    },
    CLEAR_DUNGEON(state) {
        state.dungeon = {};
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}