import * as api from '@/util/apiwrapper.js'

// everything for rooms


// initial state
const state = () => (
    {
        // RoomListEntrys of all the rooms in the dungeon
        rooms: [],
        // the currently selected room
        selectedRoom: {
            globalID: null,
            displayName: "",
            description: "",
            roomNorthGlobalID: -1,
            roomSouthGlobalID: -1,
            roomEastGlobalID: -1,
            roomWestGlobalID: -1,
            actionIDs: [],
            placedObjects: [],
        },
    }
)

// getters
const getters = {
}

// actions
const actions = {
    /**
     * @author Christian Strunk
     * Load list of rooms from the server
     * @param {*} dungeonID ID of the current dungeon
     */
    async loadShortRooms({ commit }, dungeonID) {
        var rooms = await api.getRooms(dungeonID);
        commit('SET_ROOMS', rooms);
    },
    /**
     * @author Christian Strunk
     * Clear the localy saved rooms
     */
    async clearRooms({ commit }) {
        commit('CLEAR_ROOMS');
        commit('CLEAR_SELECTEDROOM');
    },
    /**
     * @author Christian Strunk
     * Load a specific room from the server
     * @param {*} dungeonID ID of the current dungeon
     * * @param {*} roomID ID of the room
     */
    async loadSelectedRoom({ commit, rootState }, { dungeonID, roomID }) {
        if (roomID < 1) {
            commit('CLEAR_SELECTEDROOM');
            return;
        }

        var room = await api.getRoom(dungeonID, roomID);
        // create hashmap based on inventory entries
        var currentInventory = new Map(room.placedObjects.map(i => [i.objectTemplateGlobalID, i]));

        room.placedObjects = rootState.objecttemplate.objectTemplates.map(element => {
            var currentInventoryItem = currentInventory.get(element.globalID);
            if (currentInventoryItem == undefined) {
                return {objectTemplateGlobalID: element.globalID, count: 0, displayName: element.name};
            } else {
                return {objectTemplateGlobalID: element.globalID, count: currentInventoryItem.count, displayName: element.name};
            }
        });

        var currentActions = new Map(room.actionIDs.map(i => [i, i]));

        room.actionIDs = rootState.action.actions.map(element => {
            var currentAction = currentActions.get(element.globalID);
            if (currentAction == undefined) {
                return { globalID: element.globalID, enabled: false, name: element.name };
            } else {
                return { globalID: element.globalID, enabled: true, name: element.name };
            }
        });

        commit('SET_SELECTEDROOM', room);
    },
    /**
     * @author Christian Strunk
     * Send the selected Room to the server
     * @param {*} dungeonID ID of the current dungeon
     */
    async updateRoom({ state }, dungeonID) {
        var room = state.selectedRoom;
        var roomCopy = {
            globalID: room.globalID,
            displayName: room.displayName,
            description: room.description,
            roomNorthGlobalID: room.roomNorthGlobalID,
            roomSouthGlobalID: room.roomSouthGlobalID,
            roomEastGlobalID: room.roomEastGlobalID,
            roomWestGlobalID: room.roomWestGlobalID,
            actionIDs: [...room.actionIDs],
            placedObjects: [...room.placedObjects],
        };
        var actionIDs = roomCopy.actionIDs;
        actionIDs = actionIDs.filter(el => el.enabled).map(el => { return el.globalID });
        roomCopy.actionIDs = actionIDs;

        await api.changeRoom(dungeonID, roomCopy);
    },

    /**
     * @author Christian Strunk
     * create a new room with the data of selected room
     * @param {*} dungeonID ID of the current dungeon
     */
    async createRoom({ state }, dungeonID) {
        var room = state.selectedRoom;
        var roomCopy = {
            globalID: null,
            displayName: room.displayName,
            description: room.description,
            roomNorthGlobalID: room.roomNorthGlobalID,
            roomSouthGlobalID: room.roomSouthGlobalID,
            roomEastGlobalID: room.roomEastGlobalID,
            roomWestGlobalID: room.roomWestGlobalID,
            actionIDs: [...room.actionIDs],
            placedObjects: [...room.placedObjects],
        };
        var actionIDs = roomCopy.actionIDs;
        actionIDs = actionIDs.filter(el => el.enabled).map(el => { return el.globalID });
        roomCopy.actionIDs = actionIDs;
        await api.createRoom(dungeonID, roomCopy);
    },

    async removeRoom({state}, dungeonID) {
        await api.removeRoom(dungeonID, state.selectedRoom.globalID);
    }
}

// mutations
const mutations = {
    SET_ROOMS(state, rooms) {
        state.rooms = rooms;
    },
    CLEAR_ROOMS(state) {
        state.rooms = [];
    },
    SET_SELECTEDROOM(state, room) {
        state.selectedRoom = room;
    },
    CLEAR_SELECTEDROOM(state) {
        state.selectedRoom = {
            globalID: null,
            displayName: "",
            description: "",
            roomNorthGlobalID: -1,
            roomSouthGlobalID: -1,
            roomEastGlobalID: -1,
            roomWestGlobalID: -1,
            actionIDs: [],
            placedObjects: [],
        };
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}