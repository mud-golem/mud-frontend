import * as api from '@/util/apiwrapper.js'

// everything for user managment


// initial state
const state = () => (
    {
        blacklist: [],
        whitelist: [],
        dmlist: [],
        joinRequest: [],
    }
)

// getters
const getters = {
}

// actions
const actions = {
    async loadWhitelist({commit}, dungeonID) {
        var whitelist = await api.getWhitelist(dungeonID);
        commit('SET_WHITELIST', whitelist);
    },

    async loadBlacklist({commit}, dungeonID) {
        var blacklist = await api.getBlacklist(dungeonID);
        commit('SET_BLACKLIST', blacklist);
    },

    async loadDMlist({commit}, dungeonID) {
        var dmlist = await api.getDMList(dungeonID);
        commit('SET_DMLIST', dmlist);
    },

    async loadJoinRequests({commit}, dungeonID) {
        var dmlist = await api.getJoinRequests(dungeonID);
        commit('SET_JOINREQUEST', dmlist);
    }
}

// mutations
const mutations = {
    SET_WHITELIST(state, whitelist) {
        state.whitelist = whitelist;
    },
    SET_BLACKLIST(state, blacklist) {
        state.blacklist = blacklist;
    },
    SET_DMLIST(state, dmlist) {
        state.dmlist = dmlist;
    },
    SET_JOINREQUEST(state, joinRequest) {
        state.joinRequest = joinRequest;
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}